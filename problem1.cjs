
  function problem1(inventory, searchid)
  {
    if ((inventory === undefined )|| !Array.isArray(inventory)|| inventory == null){
      return []
    }
    if(searchid === undefined || !Number.isInteger(searchid)){
      return []
    }
    for (let i = 0; i < inventory.length; i++)
    {
         
      if (inventory[i].hasOwnProperty("id") &&  inventory[i].id === searchid) 
      {
              
        return inventory[i]
          
      }
      
    }
  
    
  return []
}

module.exports = problem1